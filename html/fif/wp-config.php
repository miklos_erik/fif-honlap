<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fif_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '172.17.0.1:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WZ~mEU>JP!Y=1wt[t(GNU`2duh3Li)8Ym]0s49xJWCT9ZV[8418?1adW$wH.wzGs');
define('SECURE_AUTH_KEY',  'bRuUp}Aa(~{MuQdv!q2Uow(;V2Q>.(l~u*VFj=3ODi[3c3fk5V)vN1+paZ#$./Y(');
define('LOGGED_IN_KEY',    ':~/}mB<?P:.`#<jr%?-=*j95u/+~G KB|#[XnTmn twjUZgw.QX#8X.v-$$f8 Gg');
define('NONCE_KEY',        'cuNX>;`aYivVnk<&<knNx>]6|_$exiB;0_]{q<&8pa!4>v2$pR _Wnc;xy^@f#FK');
define('AUTH_SALT',        '{E<j1W8Y-od~feK$c+fLBp_MvJ:Dlch6}X+V2KpQ[i47wv-sg*Rusn;w{3P >jLp');
define('SECURE_AUTH_SALT', '@Zx@=l4/)u{*|(54F!&(h#OzXH4y@fo;A>dVVd_LL:mefz&dC@*@XaFBl`G5u8lS');
define('LOGGED_IN_SALT',   '%,j&%-CqG0~@1$LSfX%iXPi}D&eR$g||:wb:=*]PGMVTTi=dw5cg`ZCLuk#tkAdU');
define('NONCE_SALT',       '%`a-/4qYs:23J<//XW&NFdBPR y#T^wJN[jZv2Ca0/5+O#5G#W}(}<a7EE8*oB7j');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'www.fif.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
